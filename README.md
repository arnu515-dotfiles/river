# My river configuration (WIP)

This configuration only works with the master build of [river](https://codeberg.org/river/river).

Please build river with XWayland support and copy the generated files from the `bin/` and `share/` directory to `/usr` or `~/.local` so that it is available in `$PATH`.

Clone this repository to `~/.config/river` and optionally clone my [Alacritty config](https://github.com/arnu515-dotfiles/catppuccin-hyprland/alacritty) too.

## Deps

- `alacritty`
- `wofi`
- `cliphist`
- `wl-clipboard`

